let taskArray=[]
const inputText=document.getElementById("task-input")
const PushBuutons=document.getElementById("task-push-button")
const PopButton=document.getElementById("task-pop-button")
const ShiftButton=document.getElementById("task-shift-button")
const unShiftButton=document.getElementById("task-unshift-button")
const SpliceButton=document.getElementById("task-slice-button")
const arrayUl=document.getElementById("task-ul")
const ClearButton=document.getElementById("clear-button")
const alertBox=document.getElementById("alert-box")
const alertContent=document.getElementById("alert-content")
const SearchButton=document.getElementById("task-search-button")
const searchText=document.getElementById("task-search")
const findEl=document.getElementById("find-el")

PushBuutons.addEventListener("click",function(){
    if (inputText.value!=="") {
        taskArray.push(inputText.value)
        Write(taskArray)
        localStorage.setItem("array",JSON.stringify(taskArray))
        inputText.value=""
    }
    else{
        alertContent.textContent="Lütfen alanı boş bırakmayınız."
        alertBox.classList.add("-translate-y-56")
        setTimeout(()=>{
            alertBox.classList.remove("-translate-y-56")
            alertContent.textContent=""
        },3000)
    }
})
PopButton.addEventListener("click",function(){
    taskArray.pop()
    Write(taskArray)
    localStorage.setItem("array",JSON.stringify(taskArray))
})
ShiftButton.addEventListener("click", function(){
    taskArray.shift()
    Write(taskArray)
    localStorage.setItem("array",JSON.stringify(taskArray))
})
unShiftButton.addEventListener("click",function(){
    if (inputText.value!=="") {
        taskArray.unshift(inputText.value)
        Write(taskArray)
        inputText.value=""
        localStorage.setItem("array",JSON.stringify(taskArray))
    }
    else{
        alertContent.textContent="Lütfen alanı boş bırakmayınız."
        alertBox.classList.add("-translate-y-56")
        setTimeout(()=>{
            alertBox.classList.remove("-translate-y-56")
            alertContent.textContent=""
        },3000)
    }
})
SpliceButton.addEventListener("click",function(){    
    if (inputText.value!=="") {
        if(isNaN(Number(inputText.value)) || Number(inputText.value)>taskArray.length){
            alertContent.textContent="Lütfen bir sayı giriniz."
            alertBox.classList.add("-translate-y-56")
            setTimeout(()=>{
            alertBox.classList.remove("-translate-y-56")
            alertContent.textContent=""
        },3000)
        }
        else{
            taskArray.splice(inputText.value,1)
            Write(taskArray)
            inputText.value=""
            localStorage.setItem("array",JSON.setItem(taskArray))
        }
        
    }
    else{
        alertContent.textContent="Lütfen alanı boş bırakmayınız."
        alertBox.classList.add("-translate-y-56")
        setTimeout(()=>{
            alertBox.classList.remove("-translate-y-56")
            alertContent.textContent=""
        },3000)
    }
})
ClearButton.addEventListener("click",function(){
    localStorage.clear()
    alertContent.textContent="Dizi temizlendi"
    alertBox.classList.add("-translate-y-56")
        setTimeout(()=>{
            alertBox.classList.remove("-translate-y-56")
            alertContent.textContent=""
            window.location.reload()
        },3000)
    
})
SearchButton.addEventListener("click",function(){
    findEl.innerHTML=""
    if (searchText.value!="") {
        taskArray.forEach((item,index)=>{
            if(item === searchText.value){
                listSearch=`
                <li class="text-transparent font-semibold gradient-currents bg-clip-text">
                    ${item}
                </li>
                `
                findEl.innerHTML += listSearch
            }
            else if(item!=searchText.value){
                alertContent.textContent="Aradığınız eleman bulunamadı."
                alertBox.classList.add("-translate-y-56")
                setTimeout(()=>{
                    alertBox.classList.remove("-translate-y-56")
                    alertContent.textContent=""
                },3000)
            }
        })
        
        
    }
    else{
        alertContent.textContent="Lütfen alanı boş bırakmayınız."
        alertBox.classList.add("-translate-y-56")
        setTimeout(()=>{
            alertBox.classList.remove("-translate-y-56")
            alertContent.textContent=""
        },3000)
    }
    searchText.value=""
})
function Write(content){
    let listItem=""
    for (let i = 0; i < content.length; i++) {
        listItem+=`<li class="text-transparent font-semibold gradient-currents bg-clip-text">${i} - ${content[i]}</li>` 
    }
    arrayUl.innerHTML=listItem
}
let arrayJ=JSON.parse(localStorage.getItem("array"))
if(arrayJ){
    taskArray=arrayJ
    Write(taskArray)
}
Write(taskArray)

